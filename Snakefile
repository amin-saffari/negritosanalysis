##----------------------------------##
##                                  ##
## Negrito Analysis :               ##
## Amin Saffari                     ##
## Saitou Lab (2017)                ##
##                                  ##
##----------------------------------##


from snakemake.utils import R
import sys

def _individuals():
        Names      = list()
        for name,rest in config["INDIVIDUALS"].items():
          Names.append(str(name))
        return Names

def _chromosomes():
        Items = list()
        for foo,bar in config["CHROMS"].items():
             Items.append(str(bar))
        return Items

def _AllChromosomes():
        Items = list()
        for foo,bar in config["CHROMS"].items():
             Items.append(str(bar))
        for foo,bar in config["NONECHROMS"].items():
             Items.append(str(bar))
        return Items

def _mixIndividual():
        Items = list()
        for foo,bar in config["MixedPopulation"].items():
             Items.append(str(foo))
        return Items

def _mySEPorONE():
        Items = list()
        for foo,bar in config["SEPorONE"].items():
             Items.append(str(foo))
        return Items

def _nameSexPop(wildcards):
        NameSexPop = list()
        if str(wildcards.sepORone) == "separated":
          for name,rest in config["INDIVIDUALS"].items():
            for foo,bar in config["CHROMS"].items():
                   NameSexPop.append(str(name)+"_"+str(bar)+"\t"+str(config["INDIVIDUALS"][name]["Sex"])+"\t"+str(config["INDIVIDUALS"][name]["Pop"])+str(config["INDIVIDUALS"][name]["Group"]))
          for name,rest in config["MixedPopulation"].items():
                   NameSexPop.append(str(name)+"_"+str(bar)+"\t"+str(config["MixedPopulation"][name]["Sex"])+"\t"+str(config["MixedPopulation"][name]["Pop"])+str(config["MixedPopulation"][name]["Group"]))
        
        elif str(wildcards.sepORone)=="asOne":
          for name,rest in config["INDIVIDUALS"].items():
            for foo,bar in config["CHROMS"].items():
                   NameSexPop.append(str(name)+"_"+str(bar)+"\t"+str(config["INDIVIDUALS"][name]["Sex"])+"\t"+str(config["INDIVIDUALS"][name]["Pop"]))
          for name,rest in config["MixedPopulation"].items():
                   NameSexPop.append(str(name)+"\t"+str(config["MixedPopulation"][name]["Sex"])+"\t"+config["MixedPopulation"][name]["Pop"])
          
        else:
          sys.exit("Group as "+wildcards.sepORone+" is not defined!")

        #print(NameSexPop)
        return NameSexPop
        
def _popColour(wildcards):
        PopColour = list()
        uniqPop    = list()
        if str(wildcards.sepORone) == "separated":
          for name,rest in config["INDIVIDUALS"].items():
#            for foo,bar in config["CHROMS"].items():
                   PopColour.append(config["INDIVIDUALS"][name]["Pop"]+config["INDIVIDUALS"][name]["Group"]+"\t"+config["INDIVIDUALS"][name]["Colour"])
          for name,rest in config["MixedPopulation"].items():
                   PopColour.append(config["MixedPopulation"][name]["Pop"]+config["MixedPopulation"][name]["Group"]+"\t"+config["MixedPopulation"][name]["Colour"])
        
        elif str(wildcards.sepORone) =="asOne":
          for name,rest in config["INDIVIDUALS"].items():
            if config["INDIVIDUALS"][name]["Pop"] not in uniqPop:
                   uniqPop.append(config["INDIVIDUALS"][name]["Pop"])
#            for foo,bar in config["CHROMS"].items():
                   PopColour.append(config["INDIVIDUALS"][name]["Pop"]+"\t"+config["INDIVIDUALS"][name]["Colour"])
          for name,rest in config["MixedPopulation"].items():
            if config["MixedPopulation"][name]["Pop"] not in uniqPop:
                   uniqPop.append(config["MixedPopulation"][name]["Pop"])
#            for foo,bar in config["CHROMS"].items():
                   PopColour.append(config["MixedPopulation"][name]["Pop"]+"\t"+config["MixedPopulation"][name]["Colour"])
          
        else:
          sys.exit("Group as "+wildcards.sepORone+" is not defined!")
        
        #print(PopColour)
        return PopColour

def _indiChrSexPopsGrps():
#        NameSexPopSeparate = list()
#        NameSexPopAsOne   = list()
        uniqPop    = list()
        uniqGrps   = list()
        uniqColours   = list()
        PopColour     = list()
        for name,rest in config["INDIVIDUALS"].items():
            if config["INDIVIDUALS"][name]["Group"] not in uniqGrps:
               uniqGrps.append(config["INDIVIDUALS"][name]["Group"]) 
    #        if config["INDIVIDUALS"][name]["Pop"] not in uniqPop:
               uniqPop.append(config["INDIVIDUALS"][name]["Pop"]+config["INDIVIDUALS"][name]["Group"]) 
    #        if config["INDIVIDUALS"][name]["Colour"] not in uniqGrps:
               uniqColours.append(config["INDIVIDUALS"][name]["Colour"]) 
               PopColour.append(str(config["INDIVIDUALS"][name]["Pop"])+config["INDIVIDUALS"][name]["Group"]+"\t"+str(config["INDIVIDUALS"][name]["Colour"]))
#            for foo,bar in config["CHROMS"].items():
#                NameSexPopSeparate.append(str(name)+"_"+str(bar)+"\t"+str(config["INDIVIDUALS"][name]["Sex"])+"\t"+str(config["INDIVIDUALS"][name]["Pop"])+str(config["INDIVIDUALS"][name]["Group"]))
#                NameSexPopAsOne.append(str(name)+"_"+str(bar)+"\t"+str(config["INDIVIDUALS"][name]["Sex"])+"\t"+str(config["INDIVIDUALS"][name]["Pop"]))
        for name,rest in config["MixedPopulation"].items():
            if config["MixedPopulation"][name]["Group"] not in uniqGrps:
               uniqGrps.append(config["MixedPopulation"][name]["Group"]) 
#            if config["MixedPopulation"][name]["Pop"] not in uniqPop:
               uniqPop.append(config["MixedPopulation"][name]["Pop"]+config["MixedPopulation"][name]["Group"]) 
#            if config["MixedPopulation"][name]["Colour"] not in uniqGrps:
               uniqColours.append(config["MixedPopulation"][name]["Colour"]) 
               PopColour.append(str(config["MixedPopulation"][name]["Pop"]+config["MixedPopulation"][name]["Group"])+"\t"+str(config["MixedPopulation"][name]["Colour"]))
            #for foo,bar in config["CHROMS"].items():
#            NameSexPopSeparate.append(str(name)+"\t"+str(config["MixedPopulation"][name]["Sex"])+"\t"+str(config["MixedPopulation"][name]["Pop"])+config["MixedPopulation"][name]["Group"])
#            NameSexPopAsOne.append(str(name)+"\t"+str(config["MixedPopulation"][name]["Sex"])+"\t"+str(config["MixedPopulation"][name]["Pop"]))
#        print(len(uniqGrps))
#        print(len(uniqPop))
#        print(len(uniqColours))
#        print(len(PopColour))
#        return NameSexPopSeparate,NameSexPopAsOne,uniqGrps,uniqPop,uniqColours,PopColour
        return uniqGrps,uniqPop,uniqColours,PopColour

#def _popcolour(): ## FIXME: This is not right it should be uniq
#        PopColour     = list()
        #PopGrpCol     = list()
        #PopGrp        = list()
        #uniqPopGrpCol = list()
#        for name,rest in config["INDIVIDUALS"].items():
#           PopColour.append(str(config["INDIVIDUALS"][name]["Pop"])+"\t"+str(config["INDIVIDUALS"][name]["Colour"]))
           #PopGrpCol.append(config["INDIVIDUALS"][name]["Colour"])
           #PopGrp.append(config["INDIVIDUALS"][name]["Pop"])
#        for name,rest in config["MixedPopulation"].items():
#           PopColour.append(str(config["MixedPopulation"][name]["Pop"])+"\t"+str(config["MixedPopulation"][name]["Colour"]))
           #PopGrpCol.append(config["MixedPopulation"][name]["Colour"])
           #PopGrp.append(config["MixedPopulation"][name]["Pop"])
        #uniqPopGrpCol=list(set(PopGrpCol))
        #print(str(len(uniqPopGrpCol))+" pop col")
        #uniqPopColGrpCol="\""+"\",\"".join(list(set(PopGrpCol)))+"\""
        #print(uniqPopColGrpCol)
#        return PopColour #,PopGrpCol,PopGrp #,uniqPopGrpCol

#def _grpIndividualsLen(wildcards):
#        indContain = list()
#        for name,rest in config["INDIVIDUALS"].items():
#          if wildcards.grp == config["INDIVIDUALS"][name]["Group"]:
#              indContain.append(name)
#        masks.extend(expand("{{where}}/{individual}/BCF/tmp/{individual}_{{chr}}.bed.gz".format(individual=indContain),where=WHERE, chr=CHROMS))
#        vcf.extend(expand("{{where}}/{individual}/BCF/Phased/{individual}_{{chr}}.vcf.gz".format(individual=indContain),where=WHERE,chr=CHROMS))
#        mappability_mask.extend(expand("{{mappabilityTrack}}/hs37d5_chr{{chr}}.mask.bed",mappabilityTrack=MAPMASK))
#        return len(indContain) #, mappability_mask

def _grpIndividualsMasks(wildcards):
        #indContain = list()
        mask = list()
        for name,rest in config["INDIVIDUALS"].items():
          if wildcards.grp == config["INDIVIDUALS"][name]["Group"]:
             mask.extend(expand("{where}/{individual}/BCF/tmp/{individual}_{chr}.bed.gz".format(where=wildcards.where,individual=name,chr=wildcards.chr)))
        for name,rest in config["MixedPopulation"].items():
          if wildcards.grp == config["MixedPopulation"][name]["Group"]:
             mask.extend(expand("{where}/Others/{mixed_individual}/BCF/tmp/{mixed_individual}_{chr}.bed.gz".format(where=wildcards.where,mixed_individual=name,chr=wildcards.chr)))
        return mask #lambda masks: list(map('--mask={0}'.format, masks)))

def _grpIndividualsBCFs(wildcards):
        #indContain = list()
        vcf   = list()
        for name,rest in config["INDIVIDUALS"].items():
          if wildcards.grp == config["INDIVIDUALS"][name]["Group"]:
             #indContain.append(name)
             vcf.extend(expand("{where}/{individual}/BCF/Phased/{individual}_{chr}.vcf.gz".format(where=wildcards.where,individual=name,chr=wildcards.chr)))
        for name,rest in config["MixedPopulation"].items():
          if wildcards.grp == config["MixedPopulation"][name]["Group"]:
             #vcf.extend(expand("{where}/Others/{mixed_individual}/BCF/{mixed_individual}_{chr}.vcf.gz".format(where=wildcards.where,mixed_individual=name,chr=wildcards.chr)))
             vcf.extend(expand("{where}/Others/{mixed_individual}/BCF/Phased/{mixed_individual}_{chr}.vcf.gz".format(where=wildcards.where,mixed_individual=name,chr=wildcards.chr)))
        return vcf 
##-----------------------------------------------##
## Define the Reference location and parameters  ##
##-----------------------------------------------##

REFERENCE        = config["REFERENCE"] 
SPECIES          = config["SPECIES"]
NAME             = config["NAME"]
GENOME           = config["GENOME"]
WHERE            = config["WHERE"]
FORWARDADAP      = config["FORWARDADAP"]
REVERSEADAP      = config["REVERSEADAP"]
REFPANEL         = config["REFPANEL"]
SGDP             = config["SGDP"]
ROOT             = config["ROOT"]
MAPMASK          = config["MAPMASK"]
NSNP             = range(200,1500,100)
MIG              = range(0,5)
MU               = config["MU"]
GEN              = config["GEN"]
PLTGROUP         = config["PLTGROUP"]
INDIVIDUALS      = _individuals()
MIXEDINDIVIDUALS = _mixIndividual()
EXTRACHROMS      = _AllChromosomes()
CHROMS           = _chromosomes()
SEPorONE         = _mySEPorONE()
#PopColour,PopGrpColours,PopGrp        = _popcolour()
#PopColour        = _popcolour()
#IndiChrSexPopsSeparate,IndiChrSexPopsAsOne,UniqGRPs,UniqPop,UniqPopGrpColours,PopColour   =_indiChrSexPopsGrps()
UniqGRPs,UniqPop,UniqPopGrpColours,PopColour   =_indiChrSexPopsGrps()
##-----------------------------------------------##
## Set software directories                      ##
##-----------------------------------------------##
MAPPER   = config["MAPPER"] 
SAMTOOLS = config["SAMTOOLS"] 
BCFTOOLS = config["BCFTOOLS"] 
FASTQC   = config["FASTQC"]
TRIMMER  = config["TRIMMER"]
TABIX    = config["TABIX"]
OTHERS   = config["OTHERS"]
SHAPEIT  = config["SHAPEIT"]
MSMC2    = config["MSMC2"]
MSMCTOOLS= config["MSMCTOOLS"]
HEFFALUMP= config["HEFFALUMP"]
TREEMIX  = config["TREEMIX"]
#JAVA_1_8 = config["JAVA_1_8"] #"/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.121-0.b13.el7_3.x86_64/jre/bin/java"
#PICARD = config["PICARD"] #"/usr/local/bin/picard-1.140/dist/picard.jar"
#GATK = config["GATK"] #"/usr/local/bin/GenomeAnalysisTK.jar"
#FREE = config["FREEBAYES"] #"/usr/local/bin/freebayes/bin/freebayes"
#BCFTOOLS = config["BCFTOOLS"] #"/usr/local/bin/bcftools"
#TABIX = config["TABIX"] #"/usr/local/bin/htslib-1.2.1/tabix"
#PLOTS = config["PLOTBCF"] #"/usr/local/bin/plot-vcfstats"
#BCFFILTER = config["BCFFILTER"] #"/usr/local/bin/vcffilter"
#BCFTOOLS = config["BCFTOOLS"] #"/usr/local/bin/vcftools"

##------------------------------##
## Define all the ruled         ##
##------------------------------##

#rule all:
#   input:
#      expand("{where}/{individual}/Done", where=WHERE, individual=INDIVIDUALS)


#expand("{where}/Mapped/{individual}.realn.bam", where=WHERE, population=POPULATION, individual=INDIVIDUALS),
#		   expand("{where}/Mapped/Statistics/{individual}.idx.txt", where=WHERE, population=POPULATION, individual=INDIVIDUALS),
#		   expand("{ref}{gen}/{gen}{name}.bwt",ref=REFERENCE,gen=GENOME, name=NAME),
#		   expand("{ref}{gen}/{gen}{name}.fai",ref=REFERENCE,gen=GENOME, name=NAME),
#		   expand("{where}/Mapped/{individual}.sort.bam.bai", where=WHERE, population=POPULATION, individual=INDIVIDUALS),
#		   expand("{where}/Mapped/Statistics/{individual}.flag.txt", where=WHERE, population=POPULATION, individual=INDIVIDUALS),
#		   expand("{where}/Mapped/{individual}.rgmdup.bam.bai",where=WHERE, population=POPULATION, individual=INDIVIDUALS),
#		   expand("{where}/Mapped/{individual}.rgmdup.bam.bai",where=WHERE, population=POPULATION, individual=INDIVIDUALS),
#		   expand("{where}/Mapped/{individual}.realn.bam.bai",where=WHERE, population=POPULATION, individual=INDIVIDUALS)
#

rule all:
    input:
       "report.html"

rule fastqc_raw:
    input:  
       fwd="{where}/{individual}/{individual}_R1.fastq.gz",
       rev="{where}/{individual}/{individual}_R2.fastq.gz"
    output: 
       "{where}/{individual}/QC/Raw",
       "{where}/{individual}/QC/Raw/Done"
    message: "Quality control {wildcards.individual}!"
    log:	"{output[0]}/log"
    threads: 20
    shell: "{FASTQC} -o {output[0]} -t {threads} --noextract -f fastq {input.fwd} {input.rev} && echo \"Done\n\" > {output[1]}"

rule trimming:
    input:
       fwd="{where}/{individual}/{individual}_R1.fastq.gz",
       rev="{where}/{individual}/{individual}_R2.fastq.gz"
    output:
       fwd="{where}/{individual}/Trimmed/{individual}.trimmed.R1.fastq.gz",
       rev="{where}/{individual}/Trimmed/{individual}.trimmed.R2.fastq.gz"
    message: "trimming {wildcards.individual}!"
    shell: "{TRIMMER} -a {FORWARDADAP} -A {REVERSEADAP} "+
           "--quality-cutoff 20 --minimum-length 20 --overlap=10 "+
           "-o {output.fwd} --paired-output {output.rev} {input.fwd} {input.rev}"

rule fastqc_trimmed:
    input:  
       fwd="{where}/{individual}/Trimmed/{individual}.trimmed.R1.fastq.gz",
       rev="{where}/{individual}/Trimmed/{individual}.trimmed.R2.fastq.gz"
    output: 
       "{where}/{individual}/QC/Trimmed",
       "{where}/{individual}/QC/Trimmed/Done"
    message: "Quality control {wildcards.individual}!"
    log:	"{output[0]}/log"
    threads: 20
    shell: "{FASTQC} -o {output[0]} -t {threads} --noextract -f fastq {input.fwd} {input.rev} && echo \"Done\n\" > {output[1]}"



rule MapperIndex: 
     input:  
        target="{ref}/{spe}{name}{gen}.fa"

     output: 
        targetIndex="{ref}/{spe}{name}{gen}.fa.bwt"

     message: "Creating the Bwa mem index for {wildcards.spe}"
     shell: "{MAPPER} index {input.target}"

rule Alignment:
     input:
        fwd = "{where}/{individual}/Trimmed/{individual}.trimmed.R1.fastq.gz",
        rev = "{where}/{individual}/Trimmed/{individual}.trimmed.R2.fastq.gz",
        ref = expand("{ref}/{spe}{name}{gen}.fa",ref=REFERENCE,spe=SPECIES, gen=GENOME, name=NAME)
     output:  "{where}/{individual}/Mapped/{individual}.bam"
     params:  RG = "'@RG\\tID:{individual}\\tLB:{individual}\\tPL:ILLUMINA\\tSM:{individual}'"
	threads: 20
	shell:   "{MAPPER} mem -M -R {params.RG} -t {threads} {input.ref} {input.fwd} {input.rev} | {SAMTOOLS} view -bS - -o {output}"

rule OffTargetReads:
        input:
           "{where}/{individual}/Mapped/{individual}.bam"
        output:
           "{where}/{individual}/Mapped/{individual}.offTarget.bam"
        message:" extract off-target reads"
        shell: "{SAMTOOLS} view -b -f 4 {input} > {output}"

#rule SortingByName:
#        input:
#              "{where}/{individual}/Mapped/{individual}.bam"
#        output:
#              "{where}/{individual}/Mapped/{individual}.sortByNAME.bam"
#        threads: 10
#        message: "Sorting the {input} by name"
#        shell: "{SAMTOOLS} sort -n --threads {threads} -o {output} {input}"

#rule SortingByNameCleaning:
#        input:
#           remain= "{where}/{individual}/Mapped/{individual}.sortByNAME.bam",
#           gone= "{where}/{individual}/Mapped/{individual}.bam"
#        output:
#           "{where}/{individual}/Mapped/{individual}.bam.Gone"
#        message:"remove raw bam"
#        shell: "echo \"Remove raw bam\" > {output} && rm {input.gone}"

rule Fixmate:
        input:
           reads="{where}/{individual}/Mapped/{individual}.bam"
        output:
           "{where}/{individual}/Mapped/{individual}.fixmate.bam"
        message:"Bam fixmate!"
        threads: 20
        shell: "{SAMTOOLS} fixmate --threads {threads} -O bam {input.reads} {output} "

rule FixmateCleaning:
        input:
          offTarget=   "{where}/{individual}/Mapped/{individual}.offTarget.bam",
          remain= "{where}/{individual}/Mapped/{individual}.fixmate.bam",
          gone= "{where}/{individual}/Mapped/{individual}.bam"
        output:
           "{where}/{individual}/Mapped/{individual}.bam.Gone"
        message:"remove SortByNAME"
        shell: "echo \"Remove sortByNAME\" > {output} && rm {input.gone}"


rule SortingBAM:
        input:
              chk="{where}/{individual}/Mapped/{individual}.bam.Gone",
              reads="{where}/{individual}/Mapped/{individual}.fixmate.bam"
        output:
              "{where}/{individual}/Mapped/{individual}.fixmate.sort.bam"
        threads: 20
        message: "Sorting the {input.reads}"
        shell: "{SAMTOOLS} sort --threads {threads} -o {output} {input.reads}"

rule SortingCleaning:
        input:
           remain="{where}/{individual}/Mapped/{individual}.fixmate.sort.bam",
           gone="{where}/{individual}/Mapped/{individual}.fixmate.bam"
        output:
           "{where}/{individual}/Mapped/{individual}.fixmate.bam.Gone"
        message:"remove fixmate bam"
        shell: "echo \"Remove fixmate \" > {output} && rm {input.gone}"

rule Mark_duplication:
        input:
           chk="{where}/{individual}/Mapped/{individual}.fixmate.bam.Gone",
           reads="{where}/{individual}/Mapped/{individual}.fixmate.sort.bam"
        output:
           "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam"
        message:"remove PCR duplicates"
        shell: "{SAMTOOLS} rmdup -S {input.reads}  {output} "

rule duplicateCleaning:
        input:
           remain="{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam",
           gone="{where}/{individual}/Mapped/{individual}.fixmate.sort.bam"
        output:
           "{where}/{individual}/Mapped/{individual}.fixmate.sort.bam.Gone"
        message:"remove duplicate reads bam"
        shell: "echo \"Remove duplicate reads\" > {output} && rm {input.gone}"

rule rmdupBam_index_generation:
        input: 
           chk="{where}/{individual}/Mapped/{individual}.fixmate.sort.bam.Gone",
           reads="{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam"
        output:
           "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam.bai"
        message:"generating bamIndex for .rgmdup.bam files"
        shell:	"{SAMTOOLS} index {input.reads} {output}"

rule idx_statistics_generation:
        input: 
           reads="{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam",
           readsIndex="{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam.bai"
        output:
           "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.idxstats"
        message: "generating idx statistics for rg bam files"
        shell: "{SAMTOOLS} idxstats {input.reads} > {output}"

rule flag_statistics_generation:
        input: 
           "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam"
        output:
           "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.flagstat"
        message: "generating flag statistics for rg bam files"
        threads: 20
        shell: 
           "{SAMTOOLS} flagstat --threads {threads} {input} > {output}"

#rule meanCoverage:
#       input:
#          "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam"
#       output:

rule SummaryAlignment:
       input:
          bamidxstats= "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.idxstats",
          bamflagstat= "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.flagstat"
       output:"{where}/{individual}/Mapped/Done"
       run:
         for f in output:
           with open(f, "w") as out:
                 out.write("Mapped, off-Target reads, sorted, duplicate-removed, indexed and summary-statistic is done!\n")

rule Genotyping:
       input:
          bamFile = "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam",
          ref = expand("{ref}/{spe}{name}{gen}.fa",ref=REFERENCE,spe=SPECIES, gen=GENOME, name=NAME)
       output:
          "{where}/{individual}/BCF/Unphased/{individual}.vcf.gz"
       shell:
          "{SAMTOOLS} mpileup --uncompressed --BCF --fasta-ref {input.ref} {input.bamFile} | {BCFTOOLS} call --consensus-caller --output-type z --output {output} "

rule tabix_prepare:
    input:
        "{where}/{individual}/BCF/Unphased/{individual}.vcf.gz"
    output:
        "{where}/{individual}/BCF/Unphased/{individual}.vcf.gz.tbi"
    shell:
        "{TABIX}/tabix -p vcf {input}"

rule bcftools_stats:
    input:
        vcf =  "{where}/{individual}/BCF/Unphased/{individual}.vcf.gz",
        vcfIdx= "{where}/{individual}/BCF/Unphased/{individual}.vcf.gz.tbi",
        ref = expand("{ref}/{spe}{name}{gen}.fa",ref=REFERENCE,spe=SPECIES, gen=GENOME, name=NAME)
    output:
        "{where}/{individual}/BCF/Unphased/{individual}.vcf.gz.stats"
    shell:
        "{BCFTOOLS} stats -F {input.ref} -s - {input.vcf} > {output}"

rule bcftools_plots:
    input:
         vcf = "{where}/{individual}/BCF/{individual}.vcf.gz.stats"
    output:
         tmp = "{where}/{individual}/BCF/Plots",
         summary = "{where}/{individual}/BCF/Plots/summary.pdf"
    shell:
        "{OTHERS}/plot-vcfstats -t {wildcards.individual} -p {output.tmp} {input.vcf}"

rule cleaning_plots:
    input:
         tmp = "{where}/{individual}/BCF/Plots",
         pdf = "{where}/{individual}/BCF/Plots/summary.pdf"
    output:
         cleanPDF= "{where}/{individual}/BCF/{individual}.pdf"
    shell:
         "mv {input.pdf} {output.cleanPDF} && rm -r {input.tmp}"

#rule bcftools_filter_ST:
#    input:
#        "GBCF/SNPcall_Samtools.vcf.gz"
#    output:
#        "GBCF/SNPcall_Samtools_filtered.vcf.gz"
#    shell:
#        "{BCFTOOLS} filter -O z -o {output} -s LOWQUAL -i '%QUAL>10' {input}"

rule spliting:
    input:
        combined="{where}/{individual}/BCF/Unphased/{individual}.vcf.gz"
    output:
        splited="{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz"
    shell:
        "{BCFTOOLS} view --output-type z --regions {wildcards.chr} --output-file {output.splited} {input.combined}"

rule splitCleaning:
     input:
       vcfStats  = "{where}/{individual}/BCF/{individual}.pdf",
       remain = expand("{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz",where=WHERE, individual=INDIVIDUALS,chr=EXTRACHROMS),
       gone="{where}/{individual}/BCF/Unphased/{individual}.vcf.gz",
       vcfIdx= "{where}/{individual}/BCF/Unphased/{individual}.vcf.gz.tbi"
     output:
           "{where}/{individual}/BCF/Unphased/{individual}.vcf.gz.Gone"
     message:"Splited to Chromosomes and now cleaning!"
     shell: "echo \"Splited to Chromosomes\" > {output}" # && rm {input.gone} {input.vcfIdx}"

rule tabixCHRprepare:
     input:
        "{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz"
     output:
        "{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz.tbi"
     shell:
        "{TABIX}/tabix -p vcf {input}"

rule BialellicBCF:
    input:
       vcf = "{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz",
       vcfIDX = "{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz.tbi",
       legendPanelChr = expand("{refpanel}/1000GP_Phase3_chr{{chr}}.legend.gz",refpanel=REFPANEL),
       bam = "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam"
       #depth  = "{where}/{individual}/BCF/Phased/tmp/{individual}_{chr}.fixmate.sort.rgmdup.bam.depth"
    output:
       OnlyBiallelicVcf= "{where}/{individual}/BCF/tmp/{individual}_{chr}.vcf.gz",
       bedFile = "{where}/{individual}/BCF/tmp/{individual}_{chr}.bed.gz"
    message: "excluding tri[or more]allelic sites and indels from {input.vcf}"
    run:
       import subprocess,shlex
       depth=subprocess.check_output([SAMTOOLS + ' depth -r ' + wildcards.chr + ' '+ input.bam + ' | awk \'{{sum += $3}} END {{print sum / NR}}\''],shell=True, universal_newlines=True)
       shell(BCFTOOLS +" view --exclude INDEL=1 --max-alleles 2 {input.vcf} | {MSMCTOOLS}/bamCaller.py --minConsQ 20 --minMapQ 20 --legend_file {input.legendPanelChr} \""+ depth +"\" {output.bedFile} | gzip -c  > {output.OnlyBiallelicVcf}")

#rule chrDepth:
#    input:
#       bam = "{where}/{individual}/Mapped/{individual}.fixmate.sort.rgmdup.bam"
#    output:
#       temp("{where}/{individual}/BCF/Phased/tmp/{individual}_{chr}.fixmate.sort.rgmdup.bam.depth")
#    run:
#       import subprocess,shlex
#       depth=subprocess.check_output([SAMTOOLS + ' depth -r ' + wildcards.chr + ' '+ input.bam + ' | awk \'{{sum += $3}} END {{print sum / NR}}\''],shell=True, universal_newlines=True)
#       storage.store("chrDepth", depth)
#       shell("echo \"avg length is {depth}\" > {output[0]}")

rule excludeSites:
    input:
       #filteredVariable  = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.vcf.gz",
       filteredVariable  = "{where}/{individual}/BCF/tmp/{individual}_{chr}.vcf.gz",
       geneticMapChr     = expand("{refpanel}/genetic_map_chr{{chr}}_combined_b37.txt",refpanel=REFPANEL),
       haplotypePanelChr = expand("{refpanel}/1000GP_Phase3_chr{{chr}}.hap.gz",refpanel=REFPANEL),
       legendPanelChr    = expand("{refpanel}/1000GP_Phase3_chr{{chr}}.legend.gz",refpanel=REFPANEL),
       samplePanel       = expand("{refpanel}/1000GP_Phase3.sample",refpanel=REFPANEL)
    output:
       #log        = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.log",
       #excludeSite = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.snp.strand.exclude"
       log        = "{where}/{individual}/BCF/tmp/{individual}_{chr}.log",
       excludeSite = "{where}/{individual}/BCF/tmp/{individual}_{chr}.snp.strand.exclude"
    message: "make a list of sites to exclude from the main run"
    threads: 10
    shell: 
       "{SHAPEIT} -check --thread {threads} --input-vcf {input.filteredVariable} --input-map {input.geneticMapChr} --input-ref {input.haplotypePanelChr} {input.legendPanelChr} {input.samplePanel} --output-log {output.log} || rc=$?; if [[ $rc == 1 ]]; then exit 0; else exit $?; fi"

rule shapeIt:
    input:
       #filteredVariable  = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.vcf.gz",
       filteredVariable  = "{where}/{individual}/BCF/tmp/{individual}_{chr}.vcf.gz",
       geneticMapChr     = expand("{refpanel}/genetic_map_chr{{chr}}_combined_b37.txt",refpanel=REFPANEL),
       haplotypePanelChr = expand("{refpanel}/1000GP_Phase3_chr{{chr}}.hap.gz",refpanel=REFPANEL),
       legendPanelChr    = expand("{refpanel}/1000GP_Phase3_chr{{chr}}.legend.gz",refpanel=REFPANEL),
       samplePanel       = expand("{refpanel}/1000GP_Phase3.sample",refpanel=REFPANEL),
       excludeSite        = "{where}/{individual}/BCF/tmp/{individual}_{chr}.snp.strand.exclude"
       #excludeSite        = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.snp.strand.exclude"
    output:
       #haploPHASED  = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.phased.haps.gz", 
       #samplePHASED = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.phased.samples",
       #log          = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.main.log"
       haploPHASED  = "{where}/{individual}/BCF/tmp/{individual}_{chr}.phased.haps.gz", 
       samplePHASED = "{where}/{individual}/BCF/tmp/{individual}_{chr}.phased.samples",
       log          = "{where}/{individual}/BCF/tmp/{individual}_{chr}.main.log"
    message: "Main shapeIt run"
    threads: 1 # increase this number if you have more than one individual in your sample!
    shell:
       "{SHAPEIT} --thread {threads} --input-vcf {input.filteredVariable} --input-map {input.geneticMapChr} --input-ref {input.haplotypePanelChr} {input.legendPanelChr} {input.samplePanel} --output-max {output.haploPHASED} {output.samplePHASED} --exclude-snp {input.excludeSite} --no-mcmc --output-log {output.log} "

rule convertShapeIt:
    input:
       #haploPHASED  = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.phased.haps.gz", 
       #samplePHASED = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.phased.samples"
       haploPHASED  = "{where}/{individual}/BCF/tmp/{individual}_{chr}.phased.haps.gz", 
       samplePHASED = "{where}/{individual}/BCF/tmp/{individual}_{chr}.phased.samples"
    output:
       #onlyPHASED  = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf", 
       #log          = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.convert.log"
       onlyPHASED  = "{where}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf", 
       log          = "{where}/{individual}/BCF/tmp/{individual}_{chr}.convert.log"
    message: "Convert back to phased vcf, containing only the phased sites that shapeit used"
    threads: 10
    shell:
       "{SHAPEIT} -convert --thread {threads} --input-haps {input.haploPHASED} {input.samplePHASED} --output-vcf {output.onlyPHASED} --output-log {output.log} "

rule CompressBCF:
    input:
       #flat = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf" 
       flat = "{where}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf" 
    output:
       #compress  = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf.gz" 
       compress  = "{where}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf.gz" 
    message: "compress your vcf"
    shell:
       "{BCFTOOLS} view --output-type z --output-file {output.compress} {input.flat}"

rule indexingBCF:
    input:
       #vcf = "{where}/{there}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz" 
       vcf = "{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz" 
    output:
       #compress  = "{where}/{there}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz.csi" 
       compress  = "{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz.csi" 
    message: "index your vcf"
    shell:
       "{BCFTOOLS} index -f {input.vcf} "

rule indexBCF:
    input:
       #vcf = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf.gz" 
       vcf = "{where}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf.gz" 
    output:
       #compress  = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf.gz.csi" 
       compress  = "{where}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf.gz.csi" 
    message: "index your vcf"
    shell:
       "{BCFTOOLS} index -f {input.vcf} "

rule mergeBCFs:
    input:
       #onlyPhased = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf.gz" ,
       #unphasedvcf = "{where}/{there}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz",
       #indexOnlyPhased = "{where}/{there}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf.gz.csi" ,
       #indexUnphasedvcf = "{where}/{there}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz.csi" 
       onlyPhased = "{where}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf.gz" ,
       unphasedvcf = "{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz",
       indexOnlyPhased = "{where}/{individual}/BCF/tmp/{individual}_{chr}.onlyPhased.vcf.gz.csi" ,
       indexUnphasedvcf = "{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz.csi" 
    output:
       #merged  = "{where}/{there}/{individual}/BCF/Phased/{individual}_{chr}.vcf.gz" 
       merged  = "{where}/{individual}/BCF/Phased/{individual}_{chr}.vcf.gz" 
    message: "merged your vcf"
    threads: 1
    run:
       if "Others" not in wildcards.where:
           shell("{BCFTOOLS} merge --thread {threads} --force-samples {input.unphasedvcf} {input.onlyPhased} | " + 
                 "awk ' BEGIN " + 
                      "{{OFS=\"\\t\"}} $0 ~ /^##/ {{print}} " + 
                      "$0 ~ /^#CHROM/ {{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}}  $0 !~ /^#/" + 
                 "{{ if(substr($11, 1, 3) != \"./.\") " + 
                     "{{ $10 = $11 }} " + 
                 " print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10  }}' | " +
                 "bcftools view --exclude INDEL=1 --output-type z --output-file {output.merged} ")
       else :
           shell("{BCFTOOLS} merge --thread {threads} --force-samples {input.unphasedvcf} {input.onlyPhased} | " + 
                 "awk ' BEGIN " + 
                      "{{OFS=\"\\t\"}} $0 ~ /^##/ {{print}} " + 
                      "$0 ~ /^#CHROM/ {{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}}  $0 !~ /^#/" + 
                 "{{ if(substr($11, 1, 3) != \"./.\") " + 
                     "{{ $10 = $11 }} " + 
                 " print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10  }}' | " +
                 "bcftools view --output-type z --output-file {output.merged} ")
    


rule HeffBCF:
    input:
       heffa = "{where}/Others/{mixed_individual}/Heffalump/{mixed_individual}.hef",
       twoBit =  expand("{ref}/{spe}{name}{gen}.fa.2bit",ref=REFERENCE,spe=SPECIES, gen=GENOME, name=NAME)
    output:
       vcfFile= "{where}/Others/{mixed_individual}/BCF/{mixed_individual}.vcf.gz"
#    message: "generating vcf out of heffa individual"
    shell:
       "{HEFFALUMP} vcfexport -D -r {input.twoBit} {input.heffa} | {TABIX}/bgzip -c > {output.vcfFile}"

rule tabixthem:
    input:
        "{where}/Others/{mixed_individual}/BCF/{mixed_individual}.vcf.gz"
    output:
        "{where}/Others/{mixed_individual}/BCF/{mixed_individual}.vcf.gz.tbi"
    shell:
        "{TABIX}/tabix -p vcf {input}"

rule SplitThisBullky:
    input:
        tbx="{where}/Others/{mixed_individual}/BCF/{mixed_individual}.vcf.gz.tbi",
        combined="{where}/Others/{mixed_individual}/BCF/{mixed_individual}.vcf.gz"
    output:
        vcf="{where}/Others/{mixed_individual}/BCF/Unphased/{mixed_individual}_{chr}.vcf.gz",
        bed="{where}/Others/{mixed_individual}/BCF/tmp/{mixed_individual}_{chr}.bed.gz"
#    message: "generating vcf and mask for MSMC from bulky vcf {wildcards.chr}"
    shell:
        "{BCFTOOLS} view --regions {wildcards.chr} {input.combined} | python2.7 {MSMCTOOLS}/vcfAllSiteParser.py {wildcards.chr} {output.bed} | {BCFTOOLS} view --output-type z --output-file {output.vcf}"

rule symbolicLink:
    input:
        "{where}/Others/{mixed_individual}/BCF/Unphased/{mixed_individual}_{chr,\d+}.vcf.gz"
    output:
        "{where}/Others/{mixed_individual}/BCF/tmp/{mixed_individual}_{chr,\d+}.vcf.gz"
    shell:
        "ln -s {input} {output}; touch -h {output}"

rule MultiHetSep:
    input:
       masks = _grpIndividualsMasks,
       vcfs  = _grpIndividualsBCFs,
       mappability_mask = expand("{mappabilityTrack}/hs37d5_chr{{chr}}.mask.bed",mappabilityTrack=MAPMASK)
       #masks = expand("{where}/{individual}/BCF/tmp/{individual}_{{chr}}.bed.gz",where=WHERE, individual=INDIVIDUALS), #  _grpIndividuals),
       #vcf = expand("{where}/{individual}/BCF/Phased/{individual}_{{chr}}.vcf.gz",where=WHERE, individual=INDIVIDUALS), #_grpIndividuals),
       #mappability_mask = expand("{mappabilityTrack}/hs37d5_chr{{chr}}.mask.bed",mappabilityTrack=MAPMASK)
    params:
       #withMask=lambda wildcards,input: list(map('--mask={0}'.format, input[0:len(INDIVIDUALS)]))
       withMasks=lambda wildcards,input: list(map('--mask={0}'.format, input.masks))
    output:
       #segFile= "{where}/AllNegritos/MSMC/data/grp{groups}.chr{chr}.msmc.in"
       segFile= "{where}/Results/MSMC/data/grp{grp}.chr{chr}.msmc.in"
#    message: "generating msmc input file for {input.masks} "
    shell:
       "{MSMCTOOLS}/generate_multihetsep.py {params.withMasks} --mask={input.mappability_mask} {input.vcfs} > {output.segFile}"
#    run:
#      import subprocess as sp
#
#      def chunks(l, n):
#        for i in range(0, len(l), n):
#            yield l[i:i + n]
#
#      for chunk in chunks({input.masks}.pop(), 4):
#         shell("echo {output.segFile}")
#            for sample in chunk:
#                pids.append(sp.Popen(["touch", sample]))
#
#      for i in range(0, len(INDIVIDUALS), 4):
#        shell("echo {params.withMask[i:i+4]}")



#        shell(MSMCTOOLS+"/generate_multihetsep.py {params.withMask} --mask={input.mappability_mask} {input.vcf} > {output.segFile}"

rule msmcTWO:
    input:
       indgroup = _grpIndividualsBCFs,
       segFile  = "{where}/Results/MSMC/data/grp{grp}.chr{chr}.msmc.in"
    params:
       tsPatterning= "'1*2+15*1+1*2'", #time segment patterning
       #indices= lambda wildcards, input: ','.join(map(str, range(int(float(wildcards.grp))*int(float(CHUNKs)),int(float(wildcards.grp))*int(float(CHUNKs))+len(input.indgroup)))), # make the index of each group
       indices= lambda wildcards, input: ','.join(map(str, range(0,2*len(input.indgroup)))), # make the index of each group
       outputPrefix = lambda wildcards, output: output.msmc2[:-10]
    output:
       #msmc2= "{where}/AllNegritos/MSMC/data/{groups}.chr{chr}.msmc2.out"
       msmc2= "{where}/Results/MSMC/data/grp{grp}.chr{chr}.msmc2.out.final.txt"
    message: "generating msmc output for group {wildcards.grp} with indices {params.indices} {input.segFile} "
    threads: 6 # number of chromosomes times the number of haplotype pairs
    shell:
       "{MSMC2} -t {threads} -p {params.tsPatterning} -o {params.outputPrefix} -I {params.indices} {input.segFile}"

rule msmc2EPSplot:
    input:
       msmcouts= expand("{where}/Results/MSMC/data/grp{grp}.chr{chr}.msmc2.out.final.txt",where=WHERE ,grp=UniqGRPs ,chr=CHROMS)
       #msmcdata= "{where}/AllNegritos/MSMC/data/"
    message: "generating msmc plot for {input.msmcouts} "
    output:
       plotScript = "{where}/Results/MSMC/plot/EPS.r"  ,
       EPSPlot = "{where}/Results/MSMC/plot/EPS.pdf"
    run:
         dataList = list()
         with open(output.plotScript, "a") as out:
            out.write ("list.of.packages <- c(\"ggplot2\", \"reshape2\")\n")
            out.write ("new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,\"Package\"])]\n")
            out.write ("if(length(new.packages)) install.packages(new.packages,repos=\"http://cran.ism.ac.jp/\")\n")
            out.write ("library(ggplot2)\n")
            out.write ("library(reshape2)\n")
            out.write ("mu  <- " + MU + "\n")
            out.write ("gen <- " + GEN+ "\n")
            for gr in UniqGRPs: # GROUPS are like [0,1,2,3,4,5,6,7,...] and each of them contain multiple indivisual
              for chr in CHROMS:
                 out.write ("grp_"+gr+"_"+chr+"_Dat<-read.table(\""+WHERE+"/Results/MSMC/data/grp"+gr+".chr"+chr+".msmc2.out.final.txt\", header=TRUE)\n")
                 out.write ("grp_"+gr+"_"+chr+"_Dat$PopColour<-\"" +UniqPopGrpColours[int(float(gr))]+"\"\n")
                 out.write ("grp_"+gr+"_"+chr+"_Dat$PopName<-\"" +UniqPop[int(float(gr))]+"\"\n")
                 dataList.append("grp_"+gr+"_"+chr+"_Dat")
                 #grpList.append(UniqPop[int(float(gr))])
                 #colorList.append(UniqPopGrpColours[int(float(gr))])
            out.write("newData <- melt(list("+",".join(dataList)+"), id.vars = c(\"time_index\",\"left_time_boundary\",\"right_time_boundary\",\"lambda\",\"PopColour\",\"PopName\"))\n")
            out.write("myColors<-c(\""+"\",\"".join(UniqPopGrpColours)+"\")\n")
            out.write("names(myColors)<-c(\""+"\",\"".join(UniqPop)+"\")\n")
            #out.write("pdf(file=\""+output.EPSPlot+"\")\n")
            out.write("ggplot(newData,aes(x=left_time_boundary/"+MU+"*"+GEN+",y=(1/lambda)/"+MU+",colour = PopName, group = L1))+ \n")
            out.write("geom_step()+\n")
            out.write("scale_x_log10( breaks = scales::trans_breaks(\"log10\", function(x) 10^x),\n")
            out.write("labels = scales::trans_format(\"log10\", scales::math_format(10^.x)))+\n")
              #limits = c(50000, 5000000)) +
            out.write("scale_y_continuous(labels = scales::comma,limits = c(0, 100000))+\n")
            out.write("theme_bw()+\n")
            out.write("facet_wrap(~PopName)+\n") 
            out.write("theme(legend.position=\"none\")+\n")
            out.write("#theme(legend.title = element_text(size=12, color = \"salmon\", face=\"bold\"),\n")
            out.write("#legend.justification=c(1,0),\n")
            out.write("#legend.position=c(0.95, 0.05),\n")
            out.write("#legend.background = element_blank(),\n")
            out.write("#legend.key = element_blank())+\n")
            out.write("annotation_logticks(sides = \"b\")+\n")
            out.write("scale_colour_manual(name=\"Population\", values = myColors)+ \n")
            out.write("labs(y=\"effective population size\",x=\"Years ago\",title=\"Aeta compare to others\")\n")
            out.write("ggsave(filename=\""+output.EPSPlot+"\")")
            #multiplot(p1, p2, p3, p4, cols=2)
         shell("R CMD BATCH --vanilla --slave {output.plotScript} ")
#facet_grid( myCol ~ L1)+


rule heffalump:
    input:
       vcf = "{where}/{individual}/BCF/Unphased/{individual}_{chr}.vcf.gz",
       twoBit =  expand("{ref}/{spe}{name}{gen}.fa.2bit",ref=REFERENCE,spe=SPECIES, gen=GENOME, name=NAME)
    output:
       heffa = "{where}/{individual}/Heffalump/{individual}_{chr}.hef"
    message: "vcf to hef for treemix"
    shell: "{HEFFALUMP} vcfin -o {output.heffa} -r {input.twoBit} -D -H {input.vcf}"

rule heffalump_hetfa:
    input:
       hetfa  = expand("{sgdp}/{{mixed_individual}}.ccomp.fa.rz",sgdp=SGDP),
       twoBit = expand("{ref}/{spe}{name}{gen}.fa.2bit",ref=REFERENCE,spe=SPECIES, gen=GENOME, name=NAME)
    output:
       heffa = "{where}/Others/{mixed_individual}/Heffalump/{mixed_individual}.hef"
    message: "hetfa to hef for treemix"
    shell: "{HEFFALUMP} hetfa -o {output.heffa} -r {input.twoBit} -s {input.hetfa}"

rule individuals:
    input:
       heffas = expand("{where}/{individual}/Heffalump/{individual}_{chr}.hef",where=WHERE, individual=INDIVIDUALS,chr=CHROMS),
       sgdpHeffa = expand("{where}/Others/{mixed_individual}/Heffalump/{mixed_individual}.hef",where=WHERE, mixed_individual=MIXEDINDIVIDUALS)
    params:
       tmpSepOrone = _nameSexPop
    output:
       indSeparated = "{where}/Results/Treemix/data/{sepORone}/individuals.ind"
       #indAsOne = "{where}/AllNegritos/Treemix/data/asOne/individuals.ind"
    run:
         with open(output.indSeparated, "a") as out:
            for indchrsexpopSep in params.tmpSepOrone : #IndiChrSexPopsSeparate: #INDIVIDUALS:
              out.write(indchrsexpopSep+"\n")
         #with open(output.indAsOne, "a") as out:
         #   for indchrsexpopAsOne in IndiChrSexPopsAsOne: #INDIVIDUALS:
         #     out.write(indchrsexpopAsOne+"\n")

rule colours:
    input:
       heffas = expand("{where}/{individual}/Heffalump/{individual}_{chr}.hef",where=WHERE, individual=INDIVIDUALS,chr=CHROMS),
       sgdpHeffa = expand("{where}/Others/{mixed_individual}/Heffalump/{mixed_individual}.hef",where=WHERE, mixed_individual=MIXEDINDIVIDUALS)
    params:
       tmpSepOrone = _popColour
    output:
       coloursSeparated   = "{where}/Results/Treemix/data/{sepORone}/Colour.pop"
       #coloursAsOne   = "{where}/AllNegritos/Treemix/data/asOne/Colour.pop"
    run:
         with open(output.coloursSeparated, "a") as out:
            for popco in params.tmpSepOrone:
                 out.write(popco+"\n")
         #with open(output.coloursAsOne, "a") as out:
         #   for popco in PopColour:
         #        out.write(popco+"\n")

rule makeTreemix:
    input:
       individuals = "{where}/Results/Treemix/data/{sepORone}/individuals.ind",
       twoBit =  expand("{ref}/{spe}{name}{gen}.fa.2bit",ref=REFERENCE,spe=SPECIES, gen=GENOME, name=NAME),
       heffalumps = expand("{where}/{individual}/Heffalump/{individual}_{chr}.hef",where=WHERE, individual=INDIVIDUALS,chr=CHROMS),
       m_heffalumps = expand("{where}/Others/{mixed_individual}/Heffalump/{mixed_individual}.hef",where=WHERE, mixed_individual=MIXEDINDIVIDUALS)
    output:
       tmx = "{where}/Results/Treemix/data/{sepORone}/N13_Aeta.tmx.gz"
    shell: "{HEFFALUMP} treemix -n 0 -i {input.individuals} -o {output.tmx} -r {input.twoBit} {input.heffalumps} {input.m_heffalumps}"

rule treemix:
    input:
       snp = "{where}/Results/Treemix/data/{sepORone}/N13_Aeta.tmx.gz"
    params:
       finishIt = lambda wildcards, output: output[0][:-5]
    output:
       tmx  = "{where}/Results/Treemix/data/{sepORone}/N13_Aeta.{mig}.{root}.{nSNP}.llik"
    shell:
       "{TREEMIX}/bin/treemix -bootstrap 100 -i {input.snp} -m {wildcards.mig} -root {wildcards.root} -k {wildcards.nSNP} -o {params.finishIt}"

rule drawTreemix:
    input:
       treemix = "{where}/Results/Treemix/data/{sepORone}/N13_Aeta.{mig}.{root}.{nSNP}.llik",
       colour  = "{where}/Results/Treemix/data/{sepORone}/Colour.pop"
    params:
       prefix = lambda wildcards, input: input[0][:-5]
    output:
       tmxPlot  = "{where}/Results/Treemix/plot/{sepORone}/N13_Aeta.{mig}.{root}.{nSNP}.pdf"
    run:
        
        R("""
        list.of.packages <- c(\"ColorBrewer\")
        new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,\"Package\"])]
        if(length(new.packages)) install.packages(new.packages,repos=\"http://cran.ism.ac.jp/\");
        source(\"{TREEMIX}/src/plotting_funcs.R\");
        pdf(file=\"{output.tmxPlot}\")
        plot_tree(\"{params.prefix}\", o=\"{input.colour}\",ybar = 0.1);
        dev.off()
        """)

rule report:
    input:
       #qcRaw=expand("{where}/{individual}/QC/Raw/Done", where=WHERE, individual=INDIVIDUALS),
       qcTrimmed = expand("{where}/{individual}/QC/Trimmed/Done", where=WHERE, individual=INDIVIDUALS),
       aligned   = expand("{where}/{individual}/Mapped/Done", where=WHERE, individual=INDIVIDUALS),
       #vcfStats  = expand("{where}/{individual}/BCF/{individual}.pdf",where=WHERE, individual=INDIVIDUALS),
       spliting  = expand("{where}/{individual}/BCF/Unphased/{individual}.vcf.gz.Gone",where=WHERE, individual=INDIVIDUALS),
       treemix   = expand("{where}/Results/Treemix/plot/{sepORone}/N13_Aeta.{mig}.{root}.{nSNP}.pdf", where=WHERE,sepORone=SEPorONE,mig=MIG,root=ROOT,nSNP=NSNP),
       shapeThis = expand("{where}/{individual}/BCF/Phased/{individual}_{chr}.vcf.gz",where=WHERE,individual=INDIVIDUALS,chr=CHROMS),
       shapeThat = expand("{where}/{individual}/BCF/Phased/{individual}_{chr}.vcf.gz",where=WHERE+"/Others",individual=MIXEDINDIVIDUALS,chr=CHROMS),
       #msmcEPSplot = expand("{where}/Results/MSMC/plot/EPS.r",where=WHERE)
    output:
       "report.html"
    run:
       from snakemake.utils import report
        
       report("""
        10 Negritos' analysis workflow
        ===================================

        ****Complete raw reads are in the other folders****        

        The quality of all raw reads controled by software fastqc. Forward and reverse strand adapter, 
        has been diagnosed as overrepresented sequences, {FORWARDADAP} and {REVERSEADAP} respectively. 

        Cutadapt given above adapter is used to trim this reads. Another round of fastqc has been 
        done for sanity check (i.e to test other overrepresented sequences).

        All trimmed reads were mapped (see Table T1_) to the {SPECIES}{NAME}{GENOME} reference genome.
        The aligned sequences were sorted, PCR removed and index properly by samtools.

        
        List of software which used here:
        fastqc: {FASTQC}
        cutadapt: {TRIMMER}
        bwa: {MAPPER}
        samtools, Indexing and Sort: {SAMTOOLS}
        Human reference: {REFERENCE}/{SPECIES}{NAME}{GENOME}.fa

        """, output[0], T1=input.aligned)


#rule intervals_generation:
#	input:
#		"{where}/Mapped/{population}/{individual}.rgmdup.bam"
#	output:
#		"{where}/Mapped/{population}/{individual}.rgmdup.intervals"
#	message:
#		"generating intervals file for rg bam files FooDuFafa ::Nice Elevator Jazzy song::"
#	shell:
#		"{JAVA_1_8} -Xmx40g -d64 -jar {GATK} -T RealignerTargetCreator -nt 12 -R {REF} -o {output} -I {input}" 
#
### No clue why we need it?
#rule realigning_rg_bam_files:
#	input:
#		input = "{where}/Mapped/{population}/{individual}.rgmdup.bam" ,
#		intervals = "{where}/Mapped/{population}/{individual}.rgmdup.intervals"
#	output:
#		"{where}/Mapped/{population}/{individual}.realn.bam"
#	message:"Realignment of {input.input}"
#	shell:
#		"{JAVA_1_8} -Xmx40g -d64 -jar {GATK} -T IngoneRealigner -R {REF} -targetIntervals {input.intervals} -o {output} -I {input.input} -compress 0 --maxReadsInMemory 100000"
#
## No clue why we need it?
##rule PicardDict:
##	input:
##        "{REF}"
##    output:
##        "{REF}.dict"
##    message: "Creating the Bwa mem index for {REF}"
##    shell:
##        "{JAVA_1_8} -jar {PICARD} CreateSequenceDictionary R={input} O={output} "
#
##rule BaseRecalibrator_generation:
##    input:
##        "0.MapData/{sample}.realn.bam"
##    output:
##        "0.MapData/{sample}.recal.data.table"    
##    message:"Creating a Base Recalibration"
##    shell:
##        "{JAVA_1_8} -Xmx20g -d64 -jar {GATK} -T BaseRecalibrator -nct 12 -I {input} -R {REF} -o {output}"
##
##rule PrintReads_generation:
##    input:
##        input = "0.MapData/{sample}.realn.bam" ,
##        intervals = "0.MapData/{sample}.recal.data.table" 
##    output:
##        "0.MapData/{sample}.recalibrated.bam"    
##    message:"generating printreads file for .realn.bam files"
##    shell:
##        "{JAVA_1_8} -Xmx10g -d64 -jar {GATK} -T PrintReads -nct 12 -I {input.input} -R {REF} -BQSR {input.intervals} -o {output}"
##
##rule indexing_sorted_bam3:
##    input: 
##        "0.MapData/{sample}.recalibrated.bam"
##    output:
##        "0.MapData/{sample}.recalibrated.bam.bai"
##    message: "generating bam index"
##    shell:
##        "{SAMTOOLS} index {input} {output}"
##
##End of common pipeline
#
#
#
###-----------------------------------------------##
###-----------------------------------------------##
#
#
###GATK
#
##rule GATK_STEP_HaplotypeCaller:
##    input:
##        expand("0.MapData/{sample}.recalibrated.bam" ,sample=SAMPLES)
##    output:
##        "GBCF/{sample}.vcf"
##    message:"calling variants for realigned bam files with the GATK pipeline SSSSSsssssSSSSSssSsSSSSsnaake it Baby Bromance"
##    shell:
##        "{JAVA_1_8} -Xmx5g -d64 -jar {GATK} -T HaplotypeCaller -R {REF} -I  --max_alternate_alleles 2 --emitRefConfidence GBCF -variant_index_type LINEAR -variant_index_parameter 128000 -o {output}"
##
### Changes compared with the original haplotype step ## Steph comments :
### stand_emit_conf 20 ## Be careful they mention some stand_emit_conf and stand_call_conf as default for the best practices. 
### stand_call_conf 20 
### mbq 10 
##
##rule joint_Genotyping: ##This command needs to be done to all samples at the same time#
##    input:
##        "GBCF/{sample}.vcf"
##    output:
##        "GBCF/All_GATK.vcf"
##    message:""
##    shell:
##        "ls -1 {input} | {JAVA_1_8} -cp -jar {GATK} -T GenotypeGBCFs -R {REF} --variant  -o {output}"
##
#### PLUS : Here I added the "ls -1 {input} | " step to create a list of samples it should work !
##
##rule GATK_HardFiltering_BCF:
##    input:
##        "GBCF/All_GATK.vcf"
##    output:
##        "GBCF/All_HF_GATK.vcf"
##    message:"Hard filtering the SNPs"
##    shell:
##        "{JAVA_1_8} -jar {GATK} -T VariantFiltration -R {REF} -V {input} --filterExpression QD < 2.0 || FS > 60.0 || MQ < 40.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0 --filterName my_snp_filter -o {output}"
##
##rule PASS_SNPs_Filtering_BCF:
##    input:
##        "GBCF/All_HF_GATK.vcf"
##    output:
##        "GBCF/All_HF_SNPsPASS_GATK.recode.vcf"
##    message:"calling variants for realigned bam files with the GATK pipeline SSSSSsssssSSSSSssSsSSSSsnaake it Baby Bromance"
##    shell:
##        "{vcftools} --vcf {input} --remove-ingones --remove-filtered-all --recode --recode-INFO-all --out {output}"
##
##rule GATK_recalibration_BCF:
##    input:
##        vcf = "GBCF/All_GATK.vcf" ,
##	SNPdb = "GBCF/All_HF_SNPsPASS_GATK.recode.vcf"	
##    output:
##        recal = "GBCF/recalibrate_SNP.recal" ,
##	tranches = "GBCF/recalibrate_SNP.tranches" ,
##	rscript = "GBCF/recalibrate_SNP_plots.R"
##    message:"calling variants for realigned bam files with the GATK pipeline SSSSSsssssSSSSSssSsSSSSsnaake it Baby Bromance"
##    shell:
##        "{JAVA_1_8} -Xmx5g -d64 -jar {GATK} -T VariantRecalibrator -R {REF} -input {input.vcf} - resource:HARD,known=false,training=true,truth=true,prior=10.0 {input.SNPdb} -an DP -an QD -an FS -an MQRankSum -an ReadPosRankSum -mode SNP -tranche 100.0 -tranche 99.9 -tranche 99.0 -tranche 90.0 -recalFile {output.recal} -tranchesFile {output.tranches} -rscriptFile {output.rscript}"
##
#### NEED HELP WHY SO MANY BRANCHES ?
##
##rule GATK_apply_recalibration_BCF:
##    input:
##        vcf = "GBCF/All_GATK.vcf" ,
##        recal = "GBCF/recalibrate_SNP.recal" ,
##	tranches = "GBCF/recalibrate_SNP.tranches" ,
##	rscript = "GBCF/recalibrate_SNP_plots.R"
##    output:
##        "GBCF/SNPcall_GATK_recalibrated.vcf"
##    message:"calling variants for realigned bam files with the GATK pipeline SSSSSsssssSSSSSssSsSSSSsnaake it Baby Bromance"
##    shell:
##        "{JAVA_1_8} -jar {GATK} -T ApplyRecalibration -R {REF} -input {input.vcf} -mode SNP --ts_filter_level 90 -recalFile {input.recal} -tranchesFile {input.tranches} -rscriptFile {input.rscript} -o {output}"
##
##
##rule PASS_SNPs_Final_Filtering_BCF:
##    input:
##        "GBCF/SNPcall_GATK_recalibrated.vcf"
##    output:
##        "GBCF/SNPcall_GATK_filtered.recode.vcf"
##    message:"calling variants for realigned bam files with the GATK pipeline SSSSSsssssSSSSSssSsSSSSsnaake it Baby Bromance"
##    shell:
##        "{vcftools} --vcf {input} --remove-ingones --remove-filtered-all --recode --recode-INFO-all --out {output}"
##
####Samtools
##
##
####FreeBayes
##
##rule FREE_BAYES:
##    input:
##	  expand("0.MapData/{sample}.recalibrated.bam", sample=SAMPLES)
##        #"bam_file_list.txt"   ## Should be a list with the bam localization and names! I don't know which is the best option, could be create a rule to list all the recalibrated files!     
##    output:
##        "GBCF/SNPcall_FreeBayes.vcf"
##    message:"calling variants Free Bayes Bad Bromance "
##    shell:
##        "ls -1 {input} | {FREE} -L  -f {REF} -v {output}"
##
##rule bcftools_filter_FB:
##    input:
##        "GBCF/SNPcall_FreeBayes.vcf"
##    output:
##        "GBCF/SNPcall_FreeBayes_filtered.vcf"
##    shell:
##        "{vcffilter} -f \"QUAL > 1 & QUAL / AO > 10 & SAF > 0 & SAR > 0 & RPR > 1 & RPL > 1\" {input} > {output} "
##
#### Quality Metrics on variants
### Tiv Ratio (2.1 for WGS ~2.8 for exome)
### HapMap concordance
### SNV/Ingone Counts
### Rare variant enrichment
### DP
### Q
### GQ 
#
#
#

